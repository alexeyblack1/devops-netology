Домашнее задание к занятию "3.5. Файловые системы"
+Узнайте о sparse (разряженных) файлах.

2. **Могут ли файлы, являющиеся жесткой ссылкой на один объект, иметь разные права доступа и владельца? Почему?**

Не могут. Жесткая ссылка и файл, для которой она создавалась имеют одинаковые inode ( индексный дескриптор). 
Ссылка имеет те же размер,разрешение,владелец/группа,расположение на жестком диске,дату/время, что и файл - 
различается только имя файлов. Hard link это еще одно имя для файла.

3.**Сделайте vagrant destroy на имеющийся инстанс Ubuntu. Замените содержимое Vagrantfile следующим:**

Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-20.04"
  config.vm.provider :virtualbox do |vb|
    lvm_experiments_disk0_path = "/tmp/lvm_experiments_disk0.vmdk"
    lvm_experiments_disk1_path = "/tmp/lvm_experiments_disk1.vmdk"
    vb.customize ['createmedium', '--filename', lvm_experiments_disk0_path, '--size', 2560]
    vb.customize ['createmedium', '--filename', lvm_experiments_disk1_path, '--size', 2560]
    vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', 

lvm_experiments_disk0_path]
    vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', 

lvm_experiments_disk1_path]
  end
end

+Данная конфигурация создаст новую виртуальную машину с двумя дополнительными неразмеченными дисками по 2.5 Гб.

+Используя fdisk, разбейте первый диск на 2 раздела: 2 Гб, оставшееся пространство.

+Используя sfdisk, перенесите данную таблицу разделов на второй диск.

+Соберите mdadm RAID1 на паре разделов 2 Гб.

+Соберите mdadm RAID0 на второй паре маленьких разделов.

+Создайте 2 независимых PV на получившихся md-устройствах.

+Создайте общую volume-group на этих двух PV.

+Создайте LV размером 100 Мб, указав его расположение на PV с RAID0.

+Создайте mkfs.ext4 ФС на получившемся LV.

+Смонтируйте этот раздел в любую директорию, например, /tmp/new.

+Поместите туда тестовый файл, например wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /tmp/new/test.gz.

+Прикрепите вывод lsblk.
```vagrant@vagrant:~$ lsblk
NAME                 MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT

sda                    8:0    0   64G  0 disk
├─sda1                 8:1    0  512M  0 part  /boot/efi
├─sda2                 8:2    0    1K  0 part
└─sda5                 8:5    0 63.5G  0 part
  ├─vgvagrant-root   253:0    0 62.6G  0 lvm   /
  └─vgvagrant-swap_1 253:1    0  980M  0 lvm   [SWAP]
sdb                    8:16   0  2.5G  0 disk
├─sdb1                 8:17   0    2G  0 part
│ └─md0                9:0    0    2G  0 raid1
└─sdb2                 8:18   0  511M  0 part
  └─md1                9:1    0 1018M  0 raid0
    └─vg1-lv_100     253:2    0  100M  0 lvm   /tmp/new
sdc                    8:32   0  2.5G  0 disk
├─sdc1                 8:33   0    2G  0 part
│ └─md0                9:0    0    2G  0 raid1
└─sdc2                 8:34   0  511M  0 part
  └─md1                9:1    0 1018M  0 raid0
    └─vg1-lv_100     253:2    0  100M  0 lvm   /tmp/new
```

+Протестируйте целостность файла:

root@vagrant:~# gzip -t /tmp/new/test.gz
root@vagrant:~# echo $?
0


vagrant@vagrant:~$ gzip -t /tmp/new/test.gz

vagrant@vagrant:~$ echo $?
0

+Используя pvmove, переместите содержимое PV с RAID0 на RAID1.

+Сделайте --fail на устройство в вашем RAID1 md.

vagrant@vagrant:~$ sudo mdadm --detail /dev/md0 /dev/md0
```           Version : 1.2
     Creation Time : Mon Jul 12 14:25:16 2021
        Raid Level : raid1
        Array Size : 2094080 (2045.00 MiB 2144.34 MB)
     Used Dev Size : 2094080 (2045.00 MiB 2144.34 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Mon Jul 12 15:11:05 2021
             State : clean, degraded
    Active Devices : 1
   Working Devices : 1
    Failed Devices : 1
     Spare Devices : 0

Consistency Policy : resync

              Name : vagrant:0  (local to host vagrant)
              UUID : e3eb3b27:1df7e5ec:37591383:b242803a
            Events : 19

    Number   Major   Minor   RaidDevice State
       -       0        0        0      removed
       1       8       33        1      active sync   /dev/sdc1

       0       8       17        -      faulty   /dev/sdb1
```

Подтвердите выводом dmesg, что RAID1 работает в деградированном состоянии.

[ 3173.468316] md/raid1:md0: Disk failure on sdb1, disabling device.
               md/raid1:md0: Operation continuing on 1 devices.

Протестируйте целостность файла, несмотря на "сбойный" диск он должен продолжать быть доступен:

+root@vagrant:~# gzip -t /tmp/new/test.gz
+root@vagrant:~# echo $?
0
Погасите тестовый хост, vagrant destroy.
